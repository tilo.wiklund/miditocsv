#!/usr/bin/env perl

use MIDI;
use strict;
use warnings;

my $opus = MIDI::Opus->new({ 'from_file' => $ARGV[0] });
# my $opus = MIDI::Opus->new({ 'from_file' => "/home/tilo/example.mid" });
# my $opus = MIDI::Opus->new({ 'from_file' => '/home/jesper/Research/haymoz/hayp31a.mid' });

print "track, time, duration, note, instrument, velocity \n";
my @tracks = $opus->tracks;
foreach(1..@tracks) {
    my $trackno = $_ - 1;
    my $track = $tracks[$trackno];
    my ($parti, $ticks) = MIDI::Score::events_r_to_score_r($track->events_r);
    my %ignored;
    foreach(@$parti) {
        # my @event = @$_;
        my ($type, $time, $duration, $channelno, $noteno, $velocity) = @$_;
        if($type eq "note") {
            my $note = $MIDI::number2note{$noteno};
            my $instrument = $MIDI::number2patch{$channelno};
            print "$trackno, $time, $duration, \"$note\", \"$instrument\", $velocity \n";
        } else {
            # print STDERR "Ignoring event of type $type in track $trackno.\n";
            $ignored{$type}++;
        }
    }
    for(keys %ignored) {
        my $type = $_;
        print STDERR "Ignored $ignored{$type} value(s) of type $type in track $trackno\n";
    }
}
